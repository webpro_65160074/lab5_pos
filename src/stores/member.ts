import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id: 1, name: 'มานะ มามา', tel: '0916974713' },
        { id: 2, name: 'มานี้ ตรงนี้', tel: '0886725711' }
    ])
    const currentMember = ref<Member|null>()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0){
            currentMember.value =null
        }
        currentMember.value = members.value[index]
    }
    return {
        members,currentMember
        , searchMember
    }
})
